package com.deloitte.challenge.service;

import com.deloitte.challenge.models.Activity;
import com.deloitte.challenge.models.ScheduledActivity;
import com.deloitte.challenge.parsing.ActivityParser;
import com.deloitte.challenge.parsing.ActivityParserTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ActivitySchedulerServiceTest {

    @Test
    public void testScheduleLunch() {
        ScheduledActivity duckHerding = createActivity("Duck Herding", 60, LocalTime.of(9,0));
        ScheduledActivity archery = createActivity("Archery", 45, LocalTime.of(10, 0));
        ScheduledActivity magic = createActivity("Learning Magic Tricks", 40, LocalTime.of(10, 45));
        ScheduledActivity clayShooting = createActivity("Laser Clay Shooting", 50, LocalTime.of(11, 25));
        ScheduledActivity humanTableFootball = createActivity("Human Table Football", 30, LocalTime.of(12,15));

        LinkedList<ScheduledActivity> activities = new LinkedList<>();
        activities.add(duckHerding);
        activities.add(archery);
        activities.add(magic);
        activities.add(clayShooting);
        activities.add(humanTableFootball);

        ActivitySchedulerService.addLunch(activities);

        Assert.assertEquals(6, activities.size());
        ScheduledActivity lunch = activities.get(3);
        Assert.assertEquals("Lunch Break", lunch.getActivityName());

        ScheduledActivity afterLunch1 = activities.get(4);
        // Check that it is the correct activity
        Assert.assertEquals("Laser Clay Shooting", afterLunch1.getActivityName());
        // Check that it's time is correct, shifted by 1 hour compared to initial
        Assert.assertEquals(LocalTime.of(12, 25), afterLunch1.getStartTime());
    }

    @Test
    public void testSchedulingFromFile() throws IOException {
        URL inputURL = ActivityParserTest.class.getClassLoader().getResource("activities.txt");
        File file = new File(inputURL.getFile());

        List<Activity> activities = ActivityParser.parseFile(file);

        Map<Integer, LinkedList<ScheduledActivity>> activityMap = ActivitySchedulerService.scheduleActivities(1, activities);
        LinkedList<ScheduledActivity> scheduledActivities = activityMap.get(1);

        // check that we have at least 7*60 minutes of activities scheduled, so we finish at earliest at 16
        int totalScheduledMinutes = scheduledActivities.stream().mapToInt(activity -> activity.getActivity().getDurationInMinutes()).sum();
        Assert.assertEquals(7*60, totalScheduledMinutes, 60);

        // check that the last activities end time is between 16 - 17
        ScheduledActivity scheduledActivity = scheduledActivities.get(scheduledActivities.size() - 2); // getting the second to last activity, since the last one is the team talk
        boolean endsAfterOrAt16 = scheduledActivity.getEndTime().isAfter(LocalTime.of(16,0)) || scheduledActivity.getEndTime().equals(LocalTime.of(16, 0));
        boolean endsBeforeOrAt17 = scheduledActivity.getEndTime().isBefore(LocalTime.of(17,0)) || scheduledActivity.getEndTime().equals(LocalTime.of(17, 0));
        boolean isValid = endsAfterOrAt16 && endsBeforeOrAt17;
        Assert.assertTrue(isValid);

        // check that lunch is between 11 and 12
        ScheduledActivity lunch = scheduledActivities.stream().filter(activity -> activity.getActivityName().equals("Lunch Break")).findFirst().get();
        boolean isValid2 = lunch.getStartTime().isAfter(LocalTime.of(11,00)) && lunch.getStartTime().isBefore(LocalTime.of(12,0));
        Assert.assertTrue(isValid2);

        // check that there is no time between activities, with the exception of any possible time before the team talk
        boolean found = false;
        for (int i = 1; i < scheduledActivities.size() - 1; i++) {
            ScheduledActivity actBefore = scheduledActivities.get(i - 1);
            ScheduledActivity activity = scheduledActivities.get(i);

            if (! actBefore.getEndTime().equals(activity.getStartTime())) {
                found = true;
                break;
            }
        }
        Assert.assertFalse(found);
    }

    @Test
    public void testMultipleTeams() throws IOException {
        URL inputURL = ActivityParserTest.class.getClassLoader().getResource("activities.txt");
        File file = new File(inputURL.getFile());

        List<Activity> activities = ActivityParser.parseFile(file);

        Map<Integer, LinkedList<ScheduledActivity>> activityMap = ActivitySchedulerService.scheduleActivities(2, activities);

        // Check that we haven't reused activities, with the exception of the lunch activity and the talk activity
        boolean found = false;
        LinkedList<ScheduledActivity> team1 = activityMap.get(1);
        List<Activity> team2Activities = activityMap.get(2).stream().map(ScheduledActivity::getActivity).collect(Collectors.toList());
        for (ScheduledActivity activity : team1) {
            if (activity.getActivityName().equals("Lunch Break") || activity.getActivityName().equals("Staff Motivation Presentation")) {
                continue;
            }

            if (team2Activities.contains(activity.getActivity())) {
                found = true;
                break;
            }
        }
        Assert.assertFalse(found);
    }

    private ScheduledActivity createActivity(String name, int duration, LocalTime startTime) {
        Activity activity = new Activity();
        activity.setName(name);
        activity.setDurationInMinutes(duration);

        ScheduledActivity scheduledActivity = new ScheduledActivity(startTime, activity);

        return scheduledActivity;
    }

}
