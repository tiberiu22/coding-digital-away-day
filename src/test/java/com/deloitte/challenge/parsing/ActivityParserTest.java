package com.deloitte.challenge.parsing;

import com.deloitte.challenge.exception.ParsingException;
import com.deloitte.challenge.models.Activity;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

public class ActivityParserTest {


    @Test
    public void testSprintLineParsing() throws ParsingException {
        String input = "Time Tracker sprint";

        Activity activity = ActivityParser.parseLine(input);

        Assert.assertNotNull(activity);
        // Activity should have 15 minutes
        Assert.assertEquals(15, activity.getDurationInMinutes());
    }

    @Test
    public void testMinutesLineParsing() throws ParsingException {
        String input = "Archery 45min";

        Activity activity = ActivityParser.parseLine(input);

        Assert.assertNotNull(activity);
        // Activity should have 45 minutes
        Assert.assertEquals(45, activity.getDurationInMinutes());

    }

    @Test
    public void testSingleWordNameParsing() throws ParsingException {
        String input = "Archery 45min";

        Activity activity = ActivityParser.parseLine(input);

        Assert.assertNotNull(activity);
        // Activity should "Archery" as its name
        Assert.assertEquals("Archery", activity.getName());
    }

    @Test
    public void testMultipleWordsNameParsing() throws ParsingException {
        String input = "Time Tracker sprint";

        Activity activity = ActivityParser.parseLine(input);

        Assert.assertNotNull(activity);
        Assert.assertEquals("Time Tracker", activity.getName());
    }

    @Test
    public void testReadFromFile() throws IOException {
        URL inputURL = ActivityParserTest.class.getClassLoader().getResource("activities.txt");
        File file = new File(inputURL.getFile());

        List<Activity> activities = ActivityParser.parseFile(file);

        Assert.assertFalse(activities.isEmpty());
        Assert.assertEquals(20, activities.size());

        // Check that at least the first one is the correct one
        Activity activity = activities.get(0);
        Assert.assertEquals("Duck Herding", activity.getName());
        Assert.assertEquals(60, activity.getDurationInMinutes());
    }
}
