package com.deloitte.challenge.parsing;

import com.deloitte.challenge.models.Activity;
import com.deloitte.challenge.models.ScheduledActivity;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalTime;
import java.util.LinkedList;

public class ActivityDeserializerTest {

    @Test
    public void singleMinutesActivityDeserializer() {
        ScheduledActivity scheduledActivity = createActivity("Buggy Driving", 30, LocalTime.of(10, 30));

        String activityString = ActivityDeserializer.deserializeActivity(scheduledActivity);

        Assert.assertNotNull(activityString);
        Assert.assertEquals("10:30 am : Buggy Driving 30min", activityString);
    }

    @Test
    public void singleSprintActivityDeserializer() {
        ScheduledActivity scheduledActivity = createActivity("Wine Tasting", 15, LocalTime.of(10, 30));

        String activityString = ActivityDeserializer.deserializeActivity(scheduledActivity);

        Assert.assertNotNull(activityString);
        Assert.assertEquals("10:30 am : Wine Tasting sprint", activityString);
    }

    @Test
    public void multipleActivityDeserializer() {
        ScheduledActivity scheduledActivity = createActivity("Buggy Driving", 30, LocalTime.of(10, 30));
        ScheduledActivity scheduledActivity2 = createActivity("Wine Tasting", 15, LocalTime.of(11, 00));
        LinkedList<ScheduledActivity> scheduledActivities = new LinkedList<>();
        scheduledActivities.add(scheduledActivity);
        scheduledActivities.add(scheduledActivity2);

        String activities = ActivityDeserializer.deserializeActivityList(scheduledActivities);

        Assert.assertNotNull(activities);
        String[] lines = activities.split(System.lineSeparator());

        // Check that we have 2 lines
        Assert.assertEquals(2, lines.length);
        Assert.assertEquals("10:30 am : Buggy Driving 30min", lines[0]);
        Assert.assertEquals("11:00 am : Wine Tasting sprint", lines[1]);
    }

    private ScheduledActivity createActivity(String name, int duration, LocalTime startTime) {
        Activity activity = new Activity();
        activity.setName(name);
        activity.setDurationInMinutes(duration);

        ScheduledActivity scheduledActivity = new ScheduledActivity(startTime, activity);

        return scheduledActivity;
    }
}
