package com.deloitte.challenge.exception;

public class ParsingException extends Exception {

    public ParsingException(String message) {
        super(message);
    }
}
