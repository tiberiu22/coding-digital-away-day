package com.deloitte.challenge.parsing;

import com.deloitte.challenge.exception.ParsingException;
import com.deloitte.challenge.models.Activity;
import static com.deloitte.challenge.utils.Constants.SPRINT_NAME;
import static com.deloitte.challenge.utils.Constants.SPRINT_DURATION;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ActivityParser {

    /**
     * Parses an input line and creates an instance of an @code{Activity}.
     * It looks for text like:
     * {name} {time}
     * where:
     * - name: is a multi word string
     * - time: can be either the word "sprint" or something respecting the pattern "{number}min"
     *
     * @param line to parse
     * @return activity
     * @throws ParsingException if the input is empty or doesn't respect the above conventions
     */
    public static Activity parseLine(String line) throws ParsingException {
        if (line == null || "".equals(line) || " ".equals(line)) {
            throw new ParsingException("Line is null or empty");
        }

        // Split the input by spaces
        String[] strings = line.split(" ");
        if (strings == null || strings.length == 0) {
            throw new ParsingException("Could not split line into useful information");
        }

        Activity activity = new Activity();
        // Get the last piece as that should be the time
        String timeInput = strings[strings.length - 1];
        if (SPRINT_NAME.equals(timeInput)) {
            activity.setDurationInMinutes(SPRINT_DURATION);
        }
        // Parse the time from a string like 60min
        else {
            // Strip out the non-digit part
            timeInput = timeInput.replaceAll("min","");
            activity.setDurationInMinutes(Integer.valueOf(timeInput));
        }

        String name = Arrays.stream(strings).limit(strings.length - 1).collect(Collectors.joining(" "));
        activity.setName(name);

        return activity;
    }

    public static List<Activity> parseFile(File file) throws IOException {
        List<Activity> activities = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            activities = br.lines().map(line -> {
                try {
                    return parseLine(line);
                }
                catch (ParsingException e) {
                    // ignore parsing exceptions
                    e.printStackTrace();
                }
                return null;
            }).collect(Collectors.toList());
        }

        return activities;
    }
}
