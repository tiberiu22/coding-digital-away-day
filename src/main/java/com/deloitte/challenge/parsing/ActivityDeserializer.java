package com.deloitte.challenge.parsing;

import com.deloitte.challenge.models.ScheduledActivity;

import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class ActivityDeserializer {


    /**
     * Deserializes a @code{ScheduledActivity}  producing a string following the pattern:
     * hh:mm a {activity_name} {activity_duration}.
     *
     * @param activity
     * @return
     */
    public static String deserializeActivity(ScheduledActivity activity) {
        if (activity == null) {
            throw new RuntimeException("No activity provided.");
        }

        return activity.getStartTime().format(DateTimeFormatter.ofPattern("hh:mm a"))
            + " : " + activity.getActivityName()
            + " " + activity.getActivityDurationAsString();
    }

    public static String deserializeActivityList(LinkedList<ScheduledActivity> activities) {
        if (activities == null || activities.isEmpty()) {
            throw new RuntimeException("No activities have been scheduled");
        }

        return activities.stream()
            .map(activity -> deserializeActivity(activity))
            .collect(Collectors.joining(System.lineSeparator()));
    }

}
