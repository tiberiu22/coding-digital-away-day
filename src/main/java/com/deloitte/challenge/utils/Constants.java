package com.deloitte.challenge.utils;

/**
 * Class containing different constants whose used is required in multiple places.
 */
public class Constants {

    public static final int SPRINT_DURATION = 15;
    public static final String SPRINT_NAME = "sprint";



}
