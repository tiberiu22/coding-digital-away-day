package com.deloitte.challenge.service;

import com.deloitte.challenge.models.Activity;
import com.deloitte.challenge.models.ScheduledActivity;
import com.deloitte.challenge.parsing.ActivityDeserializer;
import com.deloitte.challenge.parsing.ActivityParser;
import com.deloitte.challenge.scheduler.KnapsackProblemScheduler;
import com.deloitte.challenge.scheduler.Scheduler;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class ActivitySchedulerService {

    // We need to schedule up to 7 hours so everything finishes between 16 and 17, knowing that we have an extra "lunch" activity of 60 minutes
    private static final int TOTAL_MINUTES_AVAILABLE = 7 * 60;
    private static final int DELTA_TIME = 60;

    private static final Activity LUNCH = new Activity("Lunch Break", 60);
    private static final LocalTime MIN_LUNCH_START_TIME = LocalTime.of(11,00);
    private static final LocalTime MAX_LUNCH_START_TIME = LocalTime.of(12, 00);

    private static final LocalTime DAY_START_TIME = LocalTime.of(9, 0);
    private static final ScheduledActivity TALK = new ScheduledActivity(LocalTime.of(17,0), new Activity("Staff Motivation Presentation", 0));

    /**
     * Reads a given file and parses it to create a list of activities.
     *
     * @param input
     * @return
     * @throws IOException
     */
    public static List<Activity> parseInputFile(File input) throws IOException {
        return ActivityParser.parseFile(input);
    }

    /**
     * Creates a formatted string representing the generated schedule for a team that is used
     * as the output of the application.
     *
     * @param scheduledActivitiesPerTeam map from the number of the team to a linked list of scheduled activities
     * @return output formatted string
     */
    public static String printResult(Map<Integer, LinkedList<ScheduledActivity>> scheduledActivitiesPerTeam) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry entry : scheduledActivitiesPerTeam.entrySet()) {
            stringBuilder
                .append("Team ")
                .append(entry.getKey())
                .append(" :")
                .append(System.lineSeparator())
                .append(ActivityDeserializer.deserializeActivityList((LinkedList<ScheduledActivity>) entry.getValue()))
                .append(System.lineSeparator());
        }

        return stringBuilder.toString();
    }

    /**
     * Schedules a full day of activities for the given number of teams using the provided activity list.
     *
     * @param numTeams number of teams
     * @param activities list of activities
     * @return map between the number of the team and it's scheduled activities
     */
    public static Map<Integer, LinkedList<ScheduledActivity>> scheduleActivities(int numTeams, List<Activity> activities) {
        if (activities == null || activities.isEmpty()) {
            throw new RuntimeException("Could not find any activities to schedule!");
        }

        if (! canFillUpScheduleForAllTeams(numTeams, activities)) {
            throw new RuntimeException("Can not fill up schedule for all teams!");
        }

        Scheduler scheduler = getScheduler(activities);
        // map between a team and it's scheduled activities
        Map<Integer, LinkedList<ScheduledActivity>> teamToActivitiesMap = new HashMap<>();
        for (int i = 0 ; i < numTeams; i++) {
            LinkedList<ScheduledActivity> scheduledActivities = scheduler.scheduleActivities(DAY_START_TIME);
            addLunch(scheduledActivities);
            scheduledActivities.addLast(TALK);
            teamToActivitiesMap.put(i+1, scheduledActivities);
        }

        return teamToActivitiesMap;
    }

    /**
     * Adds the lunch activity in a list of scheduled activities.
     * NOTE: only visible for testing
     *
     * @param activities list of scheduled activities
     */
    public static void addLunch(LinkedList<ScheduledActivity> activities) {
        for (int i = 0; i < activities.size(); i++) {
            ScheduledActivity scheduledActivity = activities.get(i);
            // Find first scheduled activity that has its end time in the lunch interval
            if ((MIN_LUNCH_START_TIME.isBefore(scheduledActivity.getEndTime()) || MIN_LUNCH_START_TIME.equals(scheduledActivity.getEndTime()))
                && MAX_LUNCH_START_TIME.isAfter(scheduledActivity.getEndTime())) {
                // Add lunch after this activity
                ScheduledActivity lunch = new ScheduledActivity(scheduledActivity.getEndTime(), LUNCH);
                activities.add(i+1, lunch);

                // iterate through all activities after lunch and change their start time
                ListIterator<ScheduledActivity> listIterator = activities.listIterator(i+2);
                while (listIterator.hasNext()) {
                    ScheduledActivity activity = listIterator.next();
                    activity.setStartTime(activity.getStartTime().plus(60, ChronoUnit.MINUTES));
                }
                break;
            }
        }
    }

    /**
     * Check if the given activity list contains enough activities to fill up the day for the given number of teams.
     *
     * @param numTeams number of teams
     * @param activities list of activities
     * @return true if we can fill up the day for all teams
     */
    private static boolean canFillUpScheduleForAllTeams(int numTeams, List<Activity> activities) {
        // The maximum amount of time that we should have available
        int maxTime = numTeams * TOTAL_MINUTES_AVAILABLE;
        int activitiesTime = activities.stream().map(activity -> activity.getDurationInMinutes()).collect(Collectors.summingInt(Integer::intValue));

        // We need to check that we have at most enough activities to fill up total required time minus approx 60 min.
        return maxTime - activitiesTime < DELTA_TIME;
    }

    private static Scheduler getScheduler(List<Activity> activities) {
        return new KnapsackProblemScheduler(activities, TOTAL_MINUTES_AVAILABLE);
    }
 }
