package com.deloitte.challenge;

import com.deloitte.challenge.models.Activity;
import com.deloitte.challenge.models.ScheduledActivity;
import com.deloitte.challenge.service.ActivitySchedulerService;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ActivitySchedulerApplication {

    public static void main(String[] args) throws IOException {
        int numTeams = -1;
        try {
            numTeams = Integer.valueOf(args[0]);
        }
        catch (NumberFormatException e) {
            System.err.println("The first parameter needs to be the number representing the number of teams to schedule activities for");
            System.exit(1);
        }


        String filePath = args[1];
        File file = new File(filePath);
        if (! file.exists() || ! file.canRead()) {
            throw new RuntimeException("File doesn't exist: " + file.getPath());
        }

        List<Activity> activities = ActivitySchedulerService.parseInputFile(file);
        Map<Integer, LinkedList<ScheduledActivity>> scheduledActivities = ActivitySchedulerService.scheduleActivities(numTeams, activities);

        String resultForDisplay = ActivitySchedulerService.printResult(scheduledActivities);
        System.out.println(resultForDisplay);
    }

}
