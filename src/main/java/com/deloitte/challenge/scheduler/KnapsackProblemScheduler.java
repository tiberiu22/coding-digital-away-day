package com.deloitte.challenge.scheduler;

import com.deloitte.challenge.models.Activity;
import com.deloitte.challenge.models.ScheduledActivity;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

/**
 * This algorithm tries to schedule activities up to the maximum allocated time.
 *
 * The algorithm uses the idea of the Knapsack problem, but instead of allocating
 * objects based on value and weight, it allocates activities where we consider the
 * value = weight = duration of an activity and the maximum weight in the algorithm
 * equal to the maximum allocated time for activities. Time complexity is O(NW), N- number of items
 * and W - is the maximum capacity.
 *
 * Used for reference: https://en.wikipedia.org/wiki/Knapsack_problem
 * https://dzone.com/articles/knapsack-problem
 * https://medium.com/@ssaurel/solving-the-knapsack-problem-in-java-c985c71a7e64
 */
public class KnapsackProblemScheduler extends Scheduler {

    // In order to minimise the size of the W ( = total time to allocate) element in the Knapsack problem
    // we divide that time into smaller groups, in our case we assume that the activities duration
    // is always a multiple of 5
    private static final int DURATION_OPTIMIZATION_FACTOR = 5;

    public KnapsackProblemScheduler(List<Activity> availableActivities, int totalTime) {
        super(availableActivities, totalTime);
    }


    @Override
    protected LinkedList<ScheduledActivity> schedule(LocalTime startTime) {

        int[][] matrix = generateMaxScheduledTimeMapping(availableActivities, this.totalTimeAvailable);

        int maxTimeAllocatedInFivesOfMinutes = this.totalTimeAvailable / DURATION_OPTIMIZATION_FACTOR;
        int maxValue = matrix[availableActivities.size()][maxTimeAllocatedInFivesOfMinutes];
        int capacity = maxTimeAllocatedInFivesOfMinutes;

        LinkedList<ScheduledActivity> scheduledActivities = new LinkedList<>();

        for (int i = availableActivities.size(); i > 0 && maxValue > 0; i--) {
            if (maxValue != matrix[i-1][capacity]) {
                // add activity to solution
                Activity activity = availableActivities.get(i-1);
                scheduledActivities.add(new ScheduledActivity(startTime, activity));
                startTime = startTime.plus(activity.getDurationInMinutes(), ChronoUnit.MINUTES);
                // mark activity as being used
                addToUsed(activity);

                maxValue -= getDurationOptimisedForAlgorithm(activity);
                capacity -= getDurationOptimisedForAlgorithm(activity);
            }
        }


        return scheduledActivities;
    }

    /**
     * Create a matrix whose rows represent the index of the activity in the activities list and column's number the max scheduled time. The value
     * of the matrix's cell represents the "value" of the activity, which in our case is it's duration.
     * numbOfColumns represent the max capacity, in order to optimise this we take that the max capacity in groups of DURATION_OPTIMIZATION_FACTOR minutes
     *
     * @param activities activities to generate the matrix for
     * @param maxTimeAllocated maximum time that needs to be allocated
     * @return matrix witch optimises the used time
     */
    private int[][] generateMaxScheduledTimeMapping(List<Activity> activities, int maxTimeAllocated) {
        int numberOfItems = activities.size();

        int numbOfColumns = maxTimeAllocated / DURATION_OPTIMIZATION_FACTOR;
        int[][] matrix = new int[numberOfItems+1][numbOfColumns + 1];

        // if max allocated time is 0 then set all columns in row 0 to 0, as nothing can be scheduled
        for (int i = 0; i < numbOfColumns; i ++) {
            matrix[0][i] = 0;
        }

        // initiate matrix for the case where no activities exists -> assign first row to 0
        for (int i = 0; i < numberOfItems; i++) {
            matrix[i][0] = 0;
        }

        for (int itemNumb = 1; itemNumb <= numberOfItems; itemNumb++) {
            Activity activity = activities.get(itemNumb - 1);
            for (int maxDuration = 1; maxDuration <= numbOfColumns; maxDuration++) {
                if (getDurationOptimisedForAlgorithm(activity) <= maxDuration) {
                    matrix[itemNumb][maxDuration] = Math.max(getDurationOptimisedForAlgorithm(activity) + matrix[itemNumb - 1][maxDuration - getDurationOptimisedForAlgorithm(activity)], matrix[itemNumb - 1][maxDuration]);
                }
                else {
                    matrix[itemNumb][maxDuration] = matrix[itemNumb -1][maxDuration];
                }
            }
        }

        return matrix;
    }

    /**
     * Gets the activity's duration as the number of minute groups
     * represented by the @code{DURATION_OPTIMIZATION_FACTOR}.
     *
     * @param activity to get duration for
     * @return duration as the number of groups of minutes
     */
    private int getDurationOptimisedForAlgorithm(Activity activity) {
        return activity.getDurationInMinutes() / DURATION_OPTIMIZATION_FACTOR;
    }
}
