package com.deloitte.challenge.scheduler;

import com.deloitte.challenge.models.Activity;
import com.deloitte.challenge.models.ScheduledActivity;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Base class implementing the logic required for the activity scheduler, which respects that an activity
 * is used only once in the lifetime of the scheduler.
 */
public abstract class Scheduler {

    protected List<Activity> availableActivities;
    private List<Activity> usedActivities = new ArrayList<>();

    protected int totalTimeAvailable;

    public Scheduler(List<Activity> availableActivities, int totalTime) {
        this.availableActivities = availableActivities;
        this.totalTimeAvailable = totalTime;
    }

    /**
     * Schedule activities up to the configure time starting from a given start time;
     *
     * @param startTime when the first activity should be scheduled rom
     * @return list of scheduled activities
     */
    public LinkedList<ScheduledActivity> scheduleActivities(LocalTime startTime) {

        LinkedList<ScheduledActivity> scheduledActivities = schedule(startTime);

        cleanup();

        return scheduledActivities;
    }

    protected void addToUsed(Activity activity) {
        this.usedActivities.add(activity);
    }

    /**
     * Cleans up the @link{availableActivities} list of activities that have already been scheduled.
     */
    private void cleanup() {
        // remove all previously used activities
        if (! usedActivities.isEmpty() && ! availableActivities.isEmpty()) {
            availableActivities.removeAll(usedActivities);
            usedActivities.clear();
        }
    }

    protected abstract LinkedList<ScheduledActivity> schedule(LocalTime startTime);
}
