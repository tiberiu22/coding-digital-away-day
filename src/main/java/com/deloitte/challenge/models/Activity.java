package com.deloitte.challenge.models;

/**
 * Model class that models an activity.
 *
 */
public class Activity {

    private String name;
    private int durationInMinutes;

    public Activity() {
    }

    public Activity(String name, int durationInMinutes) {
        this.name = name;
        this.durationInMinutes = durationInMinutes;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getDurationInMinutes()
    {
        return durationInMinutes;
    }

    public void setDurationInMinutes(int durationInMinutes)
    {
        this.durationInMinutes = durationInMinutes;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "name='" + name + '\'' +
                ", durationInMinutes=" + durationInMinutes +
                '}';
    }
}
