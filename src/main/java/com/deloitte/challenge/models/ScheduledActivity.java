package com.deloitte.challenge.models;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import static com.deloitte.challenge.utils.Constants.SPRINT_DURATION;
import static com.deloitte.challenge.utils.Constants.SPRINT_NAME;

/**
 * POJO that models a scheduled activity
 */
public class ScheduledActivity {


    private LocalTime startTime;
    private Activity activity;

    public ScheduledActivity(LocalTime startTime, Activity activity) {
        this.startTime = startTime;
        this.activity = activity;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public LocalTime getEndTime() {
        return startTime.plus(activity.getDurationInMinutes(), ChronoUnit.MINUTES);
    }

    public String getActivityName() {
        return activity.getName();
    }

    /**
     * Duration of the activity as a string representation.
     *
     * @return duration as string
     */
    public String getActivityDurationAsString() {
        int duration = activity.getDurationInMinutes();
        if (duration == SPRINT_DURATION) {
            return SPRINT_NAME;
        }
        else if (duration == 0) {
            return "";
        }
        else {
            return duration + "min";
        }
    }

    @Override
    public String toString() {
        return "ScheduledActivity{" +
                "startTime=" + startTime +
                ", activity=" + activity +
                '}';
    }
}
